require 'rails_helper'

RSpec.describe Services::Registration::EnrollRequest do
  subject(:service) { Services::Registration::EnrollRequest.new(request) }

  let(:locations) { [create(:location)] }
  let(:mobile_operator) { create(:mobile_operator_with_codes) }
  let(:phone) { "#{mobile_operator.codes.first.code}1234567" }
  let(:request) { build(:request, locations: locations, phone: phone) }

  describe 'subscription request creation' do
    before(:all) do
      create(:verification_message_type)
      create(:pending_message_status)
    end

    it 'pre fills code field by verification info' do
      request[:code] = nil

      service.call!
      expect(request.code).not_to be_nil
    end

    it 'saves request' do
      service.call!
      expect(request.persisted?).to eq true
    end

    it 'creates message' do
      service.call!
      expect(request.messages).not_to be_empty
    end

    it 'assigns message type to Verification' do
      service.call!
      message = request.messages[0]
      expect(message.type_id).to eq MessageType::VERIFICATION
    end

    it 'assigns message status to Pending' do
      service.call!
      message = request.messages[0]
      expect(message.status_id).to eq MessageStatus::PENDING
    end

    it 'assigns message phone to request phone' do
      service.call!
      message = request.messages[0]
      expect(message.phone).to eq request.phone
    end

    it 'adds request code into message text' do
      service.call!
      message = request.messages[0]
      expect(message.text).to include request.code
    end

    it 'assigns message operator to request operator' do
      service.call!
      message = request.messages[0]
      expect(message.operator_id).to eq request.operator_id
    end

    it 'enqueues message' do
      expect(MessageBroker).to(receive(:enqueue_verification_message))
      service.call!
    end

    context 'when subscription request is invalid' do
      before(:each) do
        allow(request).to receive(:valid?).and_return(false)
      end

      it 'raises RecordInvalid error' do
        expect { service.call! }.to raise_error(ActiveRecord::RecordInvalid)
      end
    end

    context 'when Message saving fails' do
      before(:each) do
        allow_any_instance_of(Message).to receive(:save).and_return(false)
      end

      it 'does not save Request' do
        expect { service.call! }.to raise_error(ActiveRecord::StatementInvalid)
        expect(request.persisted?).to eq false
      end
    end
  end

  describe 'validation' do
    context 'when uses valid fields' do
      it 'passes' do
        service.call!
        expect(request.errors.messages.first).to be_nil
      end
    end

    context 'when phone absent' do
      let(:request) { build(:request, phone: nil) }
      it 'fails' do
        begin
          service.call!
        rescue
          expect(request.errors[:phone][0]).to(
            eq I18n.t('errors.messages.blank'))
        end
      end
    end

    context 'phone has less then 12 symbols' do
      let(:request) { build(:request, phone: '12345678901') }
      it 'fails' do
        begin
          service.call!
        rescue
          expect(request.errors[:phone][0]).to(
            eq I18n.t('errors.messages.wrong_length.many', count: 12))
        end
      end
    end

    context 'phone has more then 12 symbols' do
      let(:request) { build(:request, phone: '1234567890123') }
      it 'fails' do
        begin
          service.call!
        rescue
          expect(request.errors[:phone][0]).to(
            eq I18n.t('errors.messages.wrong_length.many', count: 12))
        end
      end
    end

    context 'phone contains non numeric symbols' do
      let(:request) { build(:request, phone: 'a2345t78901v') }
      it 'fails' do
        begin
          service.call!
        rescue
          expect(request.errors[:phone][0]).to(
            eq I18n.t('errors.messages.not_a_number'))
        end
      end
    end

    context 'phone not unique in subscription requests' do
      it 'fails' do
        request1 = build(:request, phone: phone, locations: locations)
        service = Services::Registration::EnrollRequest.new(request1)
        expect { service.call! }.not_to raise_error

        request2 = build(:request, phone: phone, locations: locations)
        service = Services::Registration::EnrollRequest.new(request2)
        expect { service.call! }.to raise_error
        expect(request2.errors[:phone][0]).to eq I18n.t('errors.messages.taken')
      end
    end

    context 'phone not unique in subscribers' do
      it 'fails' do
        subscriber = create(:subscriber, phone: phone)

        request = build(:request, phone: phone, locations: locations)
        service = Services::Registration::EnrollRequest.new(request)
        expect { service.call! }.to raise_error
        expect(request.errors[:phone][0]).to eq I18n.t('errors.messages.taken')
      end
    end

    context 'when phone is unsupported mobile operator' do
      before(:each) do
        create(:mobile_operator, codes: [create(:mobile_code, code: '12345')])
      end

      let(:request) do
        build(:request, locations: locations, phone: '987654321012')
      end

      it 'fails' do
        expect { service.call! }.to raise_error
        expect(request.errors[:phone][0]).to(
          eq I18n.t('errors.messages.unsupported_mobile_operator'))
      end
    end

    context 'when phone is disabled mobile operator' do
      let(:code) { '12345' }

      before(:each) do
        create(:mobile_operator, enabled: false,
                                 codes: [create(:mobile_code, code: code)])
      end

      let(:request) do
        build(:request, locations: locations, phone: "#{code}9876543")
      end

      it 'fails' do
        expect { service.call! }.to raise_error
        expect(request.errors[:phone][0]).to(
          eq I18n.t('errors.messages.unsupported_mobile_operator'))
      end
    end

    context 'when phone is enabled mobile operator' do
      let(:code) { '12345' }
      let(:mobile_operator) do
        create(:mobile_operator, enabled: true,
                                 codes: [create(:mobile_code, code: code)])
      end

      before(:each) do
        mobile_operator
      end

      let(:request) do
        build(:request, locations: locations, phone: "#{code}9876543")
      end

      it "'pre fills operator_id by operator's id'" do
        expect { service.call! }.not_to raise_error
        expect(request.operator_id).to eq mobile_operator.id
      end
    end

    context 'name absent' do
      let(:request) { build(:request, name: nil) }
      it 'fails' do
        begin
          service.call!
        rescue
          expect(request.errors[:name][0]).to eq I18n.t('errors.messages.blank')
        end
      end
    end

    context 'name has more then 8 symbols' do
      let(:request) { build(:request, name: 'abcdefghi') }
      it 'fails' do
        begin
          service.call!
        rescue
          expect(request.errors[:name][0]).to(
            eq I18n.t('errors.messages.too_long.many', count: 8))
        end
      end
    end

    context 'when locations absent' do
      let(:request) { build(:request, locations: []) }
      it 'fails' do
        expect { service.call! }.to raise_error
        error_message = I18n.t('errors.messages.blank')
        expect(request.errors[:locations][0]).to eq error_message
      end
    end
  end

  describe 'verification code' do
    let(:request) { nil }

    it 'has 8 symbols length' do
      expect(service.send(:verification_code).length).to eq 8
    end
  end
end
