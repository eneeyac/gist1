module Subscription
  class RequestsController < ApplicationController
    # GET /subscription/requests/new
    def new
      @request = Subscription::Request.new
    end

    # POST /subscription/requests
    def create
      phone = subscription_request_params[:phone]
      phone = Subscription::Request.unmask_phone(phone)
      subscription_request_params[:phone] = phone
      @request = Subscription::Request.new(subscription_request_params)

      service = ::Services::Registration::EnrollRequest.new(@request)
      service.call!

      session[:session_request_phone] = @request.phone

      redirect_to verification_subscription_requests_path
    rescue ActiveRecord::RecordInvalid
      render :new
    end

    private

    # Never trust parameters from the scary internet,
    # only allow the white list through.
    def subscription_request_params
      params.require(:subscription_request)
        .permit(:phone, :name, location_ids: [])
    end
  end
end
