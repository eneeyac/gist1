# == Schema Information
#
# Table name: subscription_requests
#
#  id                   :integer          not null, primary key
#  code                 :string(8)        not null
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#  encrypted_phone      :string
#  encrypted_phone_salt :string
#  encrypted_phone_iv   :string
#  encrypted_name       :string
#  encrypted_name_salt  :string
#  encrypted_name_iv    :string
#  hashed_phone_hash    :string           not null
#  operator_id          :integer          not null
#
# Indexes
#
#  index_subscription_requests_on_created_at         (created_at)
#  index_subscription_requests_on_hashed_phone_hash  (hashed_phone_hash) UNIQUE
#  index_subscription_requests_on_operator_id        (operator_id)
#
# Foreign Keys
#
#  fk_subscription_requests_operator_id  (operator_id => mobile_operators.id)
#

# require 'attr_hashed'

module Subscription
  class Request < ActiveRecord::Base
    MAX_NAME_LENGTH = 8

    attr_encrypted :phone, key: proc { |request| request.phone_key },
                           mode: :per_attribute_iv_and_salt
    attr_encrypted :phone_hash, prefix: 'hashed_', encryptor: Sha512Encryptor,
                                key: proc { |request| request.phone_hash_key },
                                salt: proc { |request| request.phone_hash_salt }

    attr_encrypted :name, key: proc { |request| request.name_key },
                          mode: :per_attribute_iv_and_salt

    has_many :request_locations, class_name: 'Subscription::RequestLocation',
                                 foreign_key: :subscription_request_id
    has_many :locations, through: :request_locations

    has_many :request_messages, class_name: 'Subscription::RequestMessage',
                                foreign_key: :request_id
    has_many :messages, through: :request_messages
    belongs_to :operator, class_name: 'Mobile::Operator'

    validates :phone, presence: true, on: [:create, :update]
    validates :phone, length: { is: 12 }, on: [:create, :update]
    validates :phone, numericality: { only_integer: true },
                      on: [:create, :update]
    validates :phone, mobile_operators: true, on: [:create, :update]
    validate :encrypted_uniqueness, on: [:create, :update]
    validate :subscribers_uniqueness, on: [:create, :update]

    validates :name, presence: true, on: [:create, :update]
    validates :name, length: { maximum: MAX_NAME_LENGTH },
                     on: [:create, :update]

    validates :locations, presence: true, on: [:create, :update]

    validates :code, presence: true, on: [:verify]

    before_validation :unmask_phone
    before_validation :hash_phone

    scope :created_before, ->(time) { where('created_at < :time', time: time) }

    def self.unmask_phone(phone)
      phone.gsub!(/[\s()_-]/, '') if phone.present?
    end

    def unmask_phone
      Request.unmask_phone(phone)
    end

    def hash_phone
      self.phone_hash = phone
    end

    def encrypted_uniqueness
      model = self.class
      existing_instance = model.find_by_hashed_phone_hash(hashed_phone_hash)
      message = I18n.t('errors.messages.taken')
      errors.add(:phone, message) if existing_instance.present?
    end

    def subscribers_uniqueness
      hashed_phone = Subscriber.new.encrypt(:phone_hash, phone)
      existing_instance = Subscriber.find_by_hashed_phone_hash(hashed_phone)
      message = I18n.t('errors.messages.taken')
      errors.add(:phone, message) if existing_instance.present?
    end

    def phone_key
      key = Rails.application.secrets.subscription_request_phone_encryption_key
      fail 'Key is empty' if key.blank?
      key
    end

    def name_key
      key = Rails.application.secrets.subscription_request_name_encryption_key
      fail 'Key is empty' if key.blank?
      key
    end

    def phone_hash_key
      key = Rails.application.secrets.subscription_request_phone_hash_key
      fail 'Key is empty' if key.blank?
      key
    end

    def phone_hash_salt
      salt = Rails.application.secrets.subscription_request_phone_hash_salt
      fail 'Salt is empty' if salt.blank?
      salt
    end
  end
end
