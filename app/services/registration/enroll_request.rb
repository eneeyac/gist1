module Services
  module Registration
    class EnrollRequest
      attr_reader :request

      def initialize(subscription_request)
        @request = subscription_request
      end

      def call!
        request.validate!

        add_verification_info(request)

        message = build_message
        request.messages << message

        request.save!

        MessageBroker.enqueue_verification_message(message)
      end

      private

      def add_verification_info(request)
        request.code = verification_code
      end

      def verification_code
        rand.to_s[2..9]
      end

      def build_message
        Message.new(type_id: MessageType::VERIFICATION,
                    phone: request.phone,
                    text: I18n.t('subscription.request.verification_sms',
                                 code: request.code),
                    status_id: MessageStatus::PENDING,
                    operator_id: request.operator_id)
      end
    end
  end
end
